import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import './App.css';
import MyNav from './components/MyNav';
import Video from "./components/Video";
const App = () => {
  const [videos, setVideos] = useState([]);
  const [selectedVideo, selectVideo] = useState(null);
  return (
    <Container className="p-3">
      <Router>
        <MyNav onResults={setVideos} selectedType={selectedType} selectType={selectType} />

        <Switch>
          <Route path="/channel/:channelId">
            <DetailedChannel id={selectedChannel.id}
              snippet={selectedChannel.snippet}
              statistics={selectedChannel.statistics} />
          </Route>
          <Route path="/video/:videoId">
            <DetailedVideo id={selectedVideo.id}
              snippet={selectedVideo.snippet} player={selectedVideo.player}
              statistics={selectedVideo.statistics} />
          </Route>
          <Route path="/search/channel/:search">
            <>
              {videos.map(c => {
                const {
                  title, description, thumbnails,
                  channelTitle, publishTime
                } = c.snippet;

                return (<Channel
                  channelId={c.id.channelId}
                  thumbnail={thumbnails.high}
                  description={description}
                  channelTitle={channelTitle}
                  publishTime={publishTime}
                  title={title}
                  selectChannel={selectChannel} />);
              })}
            </>
          </Route>
          <Route path="/search/video/:search">
            <>
              {videos.map(v => {
                const {
                  title, description, thumbnails,
                  channelTitle, publishTime
                } = v.snippet;

                return (<Video
                  videoId={v.id.videoId}
                  thumbnail={thumbnails.high}
                  description={description}
                  channelTitle={channelTitle}
                  publishTime={publishTime}
                  title={title}
                  selectVideo={selectVideo} />);
              })}
            </>
          </Route>
          <Route path="/">
            Il faut faire une recherche
          </Route>
        </Switch>
      </Router>
    </Container>
  );
};
export default App;
