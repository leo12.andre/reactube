import axios from 'axios';

const key = 'AIzaSyC0APaltRYNH-MLHzMt7mM9FQus_zGnHR4';
const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: { key }
})

export default fetcher;